export default {
  logo: (
    <svg
      width="32"
      height="32"
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect width="32" height="32" rx="6" fill="#272729" />
      <path
        d="M13.75 11.5V25.5"
        stroke="white"
        strokeWidth="3"
        strokeLinecap="square"
      />
      <path
        d="M6.75 10H24.75"
        stroke="white"
        strokeWidth="3"
        strokeLinecap="square"
      />
    </svg>
  ),
  project: {
    link: 'https://gitlab.com/trongdo3110/rng-workspace',
    icon: (
      <svg width="24" height="24" viewBox="0 0 256 256">
        <path
          fill="currentColor"
          d="m231.9 169.8l-94.8 65.6a15.7 15.7 0 0 1-18.2 0l-94.8-65.6a16.1 16.1 0 0 1-6.4-17.3L45 50a12 12 0 0 1 22.9-1.1L88.5 104h79l20.6-55.1A12 12 0 0 1 211 50l27.3 102.5a16.1 16.1 0 0 1-6.4 17.3Z"
        ></path>
      </svg>
    ),
  },
  docsRepositoryBase: 'https://gitlab.com/trongdo3110/rng-workspace',
  useNextSeoProps() {
    return {
      titleTemplate: '%s – Design System',
    }
  },
  footer: {
    component: <footer></footer>,
  },
}
