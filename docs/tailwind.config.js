const { tailwindConfig } =  require('@rng-workspace/config')

/** @type {import('tailwindcss').Config} */
module.exports = tailwindConfig({
  content: [
    './app/**/*.{ts,tsx,md,mdx}',
    './pages/**/*.{ts,tsx,md,mdx}',
    './node_modules/@rng-workspace/ui/**/*.js'
  ]
})
