const { tailwindConfig } = require('@rng-workspace/config')

/** @type {import('tailwindcss').Config} */
module.exports = tailwindConfig({
  corePlugins: {
    preflight: false,
  },
  content: [
    /**
     * ui component inside components folder
     */
    '../../components/**/*.{ts,tsx}'
  ]
})
