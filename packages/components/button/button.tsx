import React, { forwardRef } from 'react'
import { cx } from '@rng-workspace/lib'

import { buttonBaseClasses, buttonSizeClasses } from './button.classes'

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  size?: 'sm' | 'md' | 'lg' | 'xl'
}

export const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  ({ size = 'sm', className, children, ...rest }, ref) => {
    const classes = cx(buttonBaseClasses, buttonSizeClasses[size], className)
    return (
      <button ref={ref} className={classes} {...rest}>
        {children}
      </button>
    )
  }
)
