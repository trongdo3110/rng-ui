export const buttonBaseClasses = [
  'rng-rounded-md rng-bg-[#1964FF] rng-text-white rng-font-medium',
  // Intertractive
  'hover:rng-bg-[#0650EC] focus:rng-shadow-[0px_0px_0px_1px_#FFFFFF,0px_0px_0px_3px_#DCDCDE]',
]

export const buttonSizeClasses = {
  sm: 'rng-px-3 rng-py-2 rng-text-sm',
  md: 'rng-py-2.5 rng-px-3.5 rng-text-sm',
  lg: 'rng-py-[11px] rng-px-3.5 rng-text-base rng-leading-5.5',
  xl: 'rng-py-3 rng-px-4 rng-text-lg rng-leading-6',
}
