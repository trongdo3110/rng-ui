export default function AboutPage() {
  return (
    <section>
      <h1 className="font-bold text-3xl font-serif">About Me</h1>
      <p className="my-5 text-neutral-800 dark:text-neutral-200">
        I’m a software engineer specializing in JavaScript, TypeScript & ReactJs.
      </p>
      <p className="my-5 text-neutral-800 dark:text-neutral-200">I’m currently based in VietNam and work at Cốc Cốc as a frontend engineer.</p>
    </section>
  );
}
