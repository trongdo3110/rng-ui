import chalk from 'chalk';
import fs from 'fs';
import path from 'path';

import fileDirName from './file-dir-name.js'

const { __dirname } = fileDirName(import.meta);

const currentDirectory = __dirname;
const root = path.join(__dirname, '../');

const getComponentPath = (componentName) => path.join(root, `packages/ui/src/components/${componentName}`);

const rngUIPackages = path.join(root, 'packages/ui/src');

function logError(message) {
  // eslint-disable-next-line no-console
  console.log(chalk.red(`❌  Error: ${message}`));
}

function logSuccess(message) {
  // eslint-disable-next-line no-console
  console.log(chalk.green(`✅ ${message}`));
}

async function generateFile({ componentName, outputPath, template, log }) {
  await fs.promises.writeFile(
    outputPath,
    (await fs.promises.readFile(path.join(currentDirectory, template), 'utf-8'))
      .replace(/ComponentName/g, componentName)
      .replace(/componentname/g, componentName.toLowerCase()),
  );

  logSuccess(log);
}

async function generateDirectory({ componentName, log }) {
  const dir = getComponentPath(componentName.toLowerCase());

  if (!fs.existsSync(dir)) {
    await fs.promises.mkdir(dir);
    await fs.promises.mkdir(`${dir}/src`);
    logSuccess(log);
  }
}

async function generateComponentFiles(componentName) {
  /**
   * Generate directory
   */
  await generateDirectory({
    componentName,
    log: 'Generated component directory',
  });

  const dir = getComponentPath(componentName);
  const srcDir = `${dir}/src`;

  return await Promise.all([
    generateFile({
      componentName,
      outputPath: path.join(dir, 'README.md'),
      template: 'templates/README.md',
      log: 'Generated ReadMe',
    }),
    generateFile({
      componentName,
      outputPath: path.join(srcDir, 'index.ts'),
      template: 'templates/index.ts',
      log: 'Generated index',
    }),
    generateFile({
      componentName,
      outputPath: path.join(srcDir, `${componentName}.tsx`),
      template: 'templates/ComponentName.tsx',
      log: 'Generated React component',
    }),
    generateFile({
      componentName,
      outputPath: path.join(srcDir, `${componentName}.classes.ts`),
      template: 'templates/ComponentName.classes.ts',
      log: 'Generated classes component',
    }),
    generateFile({
      componentName,
      outputPath: path.join(dir, 'package.json'),
      template: 'templates/package.json',
      log: 'Generated package.json',
    })
  ]);
}

(async function generateComponent() {
  console.log('rngUIPackages', rngUIPackages)
  const [componentName] = process.argv.slice(2);

  if (!componentName) {
    logError('Pass in a component name');
    return;
  }

  if (!componentName.match(/^[$A-Z_][0-9A-Z_$]*$/i)) {
    logError('Pass in a valid component name');
    return;
  }

  await generateComponentFiles(componentName);

  logSuccess(`${componentName} was generated`);
})();
